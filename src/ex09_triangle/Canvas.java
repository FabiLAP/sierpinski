package ex09_triangle;

import javax.swing.*;

import java.awt.*;
import java.awt.event.ComponentEvent;
import java.awt.event.ComponentListener;
import java.awt.geom.*;
import java.util.Random;

/**
 * Class Canvas - a class to allow for simple graphical 
 * drawing on a canvas.
 * 
 * @author Fabian B�nsch
 * @version 2013.05.30
 */

public class Canvas  implements ComponentListener
{
    private JFrame frame;
    private CanvasPane canvas;
    private Graphics2D graphic;
    private Color backgroundColor;
    private Image canvasImage;
    
    /**
     * Create a Canvas with default height, width and background color 
     * (300, 300, white).
     * @param title  title to appear in Canvas Frame     
     */
    public Canvas(String title)
    {
        this(title, 800, 500, Color.white);        
    }

    /**
     * Create a Canvas with default background color (white).
     * @param title  title to appear in Canvas Frame
     * @param width  the desired width for the canvas
     * @param height  the desired height for the canvas
     */
    public Canvas(String title, int width, int height)
    {
        this(title, width, height, Color.white);
    }

    /**
     * Create a Canvas.
     * @param title  title to appear in Canvas Frame
     * @param width  the desired width for the canvas
     * @param height  the desired height for the canvas
     * @param bgClour  the desired background color of the canvas
     */
    public Canvas(String title, int width, int height, Color bgColor)
    {
        frame = new JFrame();
        canvas = new CanvasPane();
        frame.setContentPane(canvas);
        frame.setTitle(title);
        canvas.setPreferredSize(new Dimension(width, height));
        backgroundColor = bgColor;
        //################################# Added Listener
        canvas.addComponentListener(this);
        //################################# Added termination!!
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frame.pack();
        setVisible(true);
        //######################################################################
        
        setForegroundColor(Color.black);
//        drawRectingularTriangleAsShape(300, 300, 100);
        
        recursiveEquilateralTriangles(5, width, height, 600);
        
//        recursiveTriangles(	6,			//levels
//				600/2, 0, 	//oben
//				0, 500, 	//links
//				600, 500);	//rechts

    }

    /**
     * Set the canvas visibility and brings canvas to the front of screen
     * when made visible. This method can also be used to bring an already
     * visible canvas to the front of other windows.
     * @param visible  boolean value representing the desired visibility of
     * the canvas (true or false) 
     */
    public void setVisible(boolean visible)
    {
        if(graphic == null) {
            // first time: instantiate the offscreen image and fill it with
            // the background color
            Dimension size = canvas.getSize();
            canvasImage = canvas.createImage(size.width, size.height);
            graphic = (Graphics2D)canvasImage.getGraphics();
            graphic.setColor(backgroundColor);
            graphic.fillRect(0, 0, size.width, size.height);
            //########################
            graphic.setColor(Color.white);
        }
        frame.setVisible(true);
    }

    /**
     * Provide information on visibility of the Canvas.
     * @return  true if canvas is visible, false otherwise
     */
    public boolean isVisible()
    {
        return frame.isVisible();
    }

    /**
     * Draw the outline of a given shape onto the canvas.
     * @param  shape  the shape object to be drawn on the canvas
     */
    public void draw(Shape shape)
    {
        graphic.draw(shape);
        canvas.repaint();
    }
 
    /**
     * Fill the internal dimensions of a given shape with the current 
     * foreground color of the canvas.
     * @param  shape  the shape object to be filled 
     */
    public void fill(Shape shape)
    {
        graphic.fill(shape);
        canvas.repaint();
    }

    /**
     * Fill the internal dimensions of the given circle with the current 
     * foreground color of the canvas.
     */
    public void fillCircle(int xPos, int yPos, int diameter)
    {
        Ellipse2D.Double circle = new Ellipse2D.Double(xPos, yPos, diameter, diameter);
        fill(circle);
    }

    /**
     * Fill the internal dimensions of the given rectangle with the current 
     * foreground color of the canvas. This is a convenience method. A similar 
     * effect can be achieved with the "fill" method.
     */
    public void fillRectangle(int xPos, int yPos, int width, int height)
    {
        fill(new Rectangle(xPos, yPos, width, height));
    }

    /**
     * Erase the whole canvas.
     */
    public void erase()
    {
        Color original = Color.white;
        graphic.setColor(Color.white);
        Dimension size = canvas.getSize();
        graphic.fill(new Rectangle(0, 0, size.width, size.height));
        graphic.setColor(original);
        canvas.repaint();
    }

    /**
     * Erase the internal dimensions of the given circle. This is a 
     * convenience method. A similar effect can be achieved with
     * the "erase" method.
     */
    public void eraseCircle(int xPos, int yPos, int diameter)
    {
        Ellipse2D.Double circle = new Ellipse2D.Double(xPos, yPos, diameter, diameter);
        erase(circle);
    }

    /**
     * Erase the internal dimensions of the given rectangle. This is a 
     * convenience method. A similar effect can be achieved with
     * the "erase" method.
     */
    public void eraseRectangle(int xPos, int yPos, int width, int height)
    {
        erase(new Rectangle(xPos, yPos, width, height));
    }

    /**
     * Erase a given shape's interior on the screen.
     * @param  shape  the shape object to be erased 
     */
    public void erase(Shape shape)
    {
        Color original = graphic.getColor();
        graphic.setColor(backgroundColor);
        graphic.fill(shape);              // erase by filling background color
        graphic.setColor(original);
        canvas.repaint();
    }

    /**
     * Erases a given shape's outline on the screen.
     * @param  shape  the shape object to be erased 
     */
    public void eraseOutline(Shape shape)
    {
        Color original = graphic.getColor();
        graphic.setColor(backgroundColor);
        graphic.draw(shape);  // erase by drawing background color
        graphic.setColor(original);
        canvas.repaint();
    }

    /**
     * Draws an image onto the canvas.
     * @param  image   the Image object to be displayed 
     * @param  x       x co-ordinate for Image placement 
     * @param  y       y co-ordinate for Image placement 
     * @return  returns boolean value representing whether the image was 
     *          completely loaded 
     */
    public boolean drawImage(Image image, int x, int y)
    {
        boolean result = graphic.drawImage(image, x, y, null);
        canvas.repaint();
        return result;
    }

    /**
     * Draws a String on the Canvas.
     * @param  text   the String to be displayed 
     * @param  x      x co-ordinate for text placement 
     * @param  y      y co-ordinate for text placement
     */
    public void drawString(String text, int x, int y)
    {
        graphic.drawString(text, x, y);   
        canvas.repaint();
    }

    /**
     * Erases a String on the Canvas.
     * @param  text     the String to be displayed 
     * @param  x        x co-ordinate for text placement 
     * @param  y        y co-ordinate for text placement
     */
    public void eraseString(String text, int x, int y)
    {
        Color original = graphic.getColor();
        graphic.setColor(backgroundColor);
        graphic.drawString(text, x, y);   
        graphic.setColor(original);
        canvas.repaint();
    }

    /**
     * Draws a line on the Canvas.
     * @param  x1   x co-ordinate of start of line 
     * @param  y1   y co-ordinate of start of line 
     * @param  x2   x co-ordinate of end of line 
     * @param  y2   y co-ordinate of end of line 
     */
    public void drawLine(int x1, int y1, int x2, int y2)
    {
        graphic.drawLine(x1, y1, x2, y2);   
        canvas.repaint();
    }
    
    public void drawTriangle(int x1, int y1, int x2, int y2, int x3, int y3){
    	graphic.drawLine(x1, y1, x2, y2);   
    	graphic.drawLine(x2, y2, x3, y3);   
    	graphic.drawLine(x3, y3, x1, y1);   
        canvas.repaint();
    }
    
    public void drawTriangleAsShape(int x1, int y1, int x2, int y2, int x3, int y3){
    	int[]xpos={x1,x2,x3};
    	int[]ypos={y1,y2,y3};
    	int n = 3;
    	Polygon triangle = new Polygon(xpos,ypos,n);
    	fill(triangle);
    }

    /**
     * Sets the foreground color of the Canvas.
     * @param  newColor   the new color for the foreground of the Canvas 
     */
    public void setForegroundColor(Color newColor)
    {
        graphic.setColor(newColor);
    }

    /**
     * Returns the current color of the foreground.
     * @return   the color of the foreground of the Canvas 
     */
    public Color getForegroundColor()
    {
        return graphic.getColor();
    }

    /**
     * Sets the background color of the Canvas.
     * @param  newColor   the new color for the background of the Canvas 
     */
    public void setBackgroundColor(Color newColor)
    {
        backgroundColor = newColor;   
        graphic.setBackground(newColor);
    }

    /**
     * Returns the current color of the background
     * @return   the color of the background of the Canvas 
     */
    public Color getBackgroundColor()
    {
        return backgroundColor;
    }

    /**
     * changes the current Font used on the Canvas
     * @param  newFont   new font to be used for String output
     */
    public void setFont(Font newFont)
    {
        graphic.setFont(newFont);
    }

    /**
     * Returns the current font of the canvas.
     * @return     the font currently in use
     **/
    public Font getFont()
    {
        return graphic.getFont();
    }

    /**
     * Sets the size of the canvas.
     * @param  width    new width 
     * @param  height   new height 
     */
    public void setSize(int width, int height)
    {
        canvas.setPreferredSize(new Dimension(width, height));
        Image oldImage = canvasImage;
        canvasImage = canvas.createImage(width, height);
        graphic = (Graphics2D)canvasImage.getGraphics();
        graphic.drawImage(oldImage, 0, 0, null);
        frame.pack();
    }

    /**
     * Returns the size of the canvas.
     * @return     The current dimension of the canvas
     */
    public Dimension getSize()
    {
        return canvas.getSize();
    }

    /**
     * Waits for a specified number of milliseconds before finishing.
     * This provides an easy way to specify a small delay which can be
     * used when producing animations.
     * @param  milliseconds  the number 
     */
    public void wait(int milliseconds)
    {
        try
        {
            Thread.sleep(milliseconds);
        } 
        catch (InterruptedException e)
        {
            // ignoring exception at the moment
        }
    }

    /************************************************************************
     * Inner class CanvasPane - the actual canvas component contained in the
     * Canvas frame. This is essentially a JPanel with added capability to
     * refresh the image drawn on it.
     */
    private class CanvasPane extends JPanel
    {
        public void paint(Graphics g)
        {
            g.drawImage(canvasImage, 0, 0, null);
        }
    }

    //###########################  implementing the methods from interface
    
	@Override
	public void componentHidden(ComponentEvent e) {
		// TODO Auto-generated method stub		
	}

	@Override
	public void componentMoved(ComponentEvent e) {
		// TODO Auto-generated method stub
	}

	/*
	 * This method gets started by resizing the canvas
	 * it fetches the new dimensions from the passed ComponentEvent
	 * and pass these to the changingSize() method
	 * 
	 * @see java.awt.event.ComponentListener#componentResized(java.awt.event.ComponentEvent)
	 */
	@Override
	public void componentResized(ComponentEvent e) {
		// TODO Auto-generated method stub
		int newHeight=e.getComponent().getHeight();
		int newWidth=e.getComponent().getWidth();
//		System.out.print("Height :" + newHeight); System.out.println(" , Width: "+newWidth);
		changingSize(newWidth, newHeight);	
	}

	@Override
	public void componentShown(ComponentEvent e) {
		// TODO Auto-generated method stub		
	}
	
	/*
	 * TODO
	 */
	public void drawRectingularTriangleAsShape(int x, int y, int length){
		//position of first/left point (x2/y2) should be represented by x and y from argument (left vertices)
		//calculate the other 2 points for passing them to the drawTriangleAsShape() method
		// http://de.wikipedia.org/wiki/Gleichseitiges_Dreieck
		
		//right point
		int x3 = x + length;
		int y3 = y;
		
		//top point		
		int x1 = x+(length/2);
		int y1 = (int) (y-(Math.sqrt(3)*length/2));
		
		drawTriangleAsShape(x1, y1, x, y, x3, y3);
	}
	
	public void recursiveEquilateralTriangles(int n, int x, int y, int length){
		//position of first/left point (x2/y2) should be represented by x and y from argument (left vertices)
		//calculate the other 2 points for passing them to the drawTriangleAsShape() method
		// http://de.wikipedia.org/wiki/Gleichseitiges_Dreieck
		
		//right point
		int x3 = x + length;
		int y3 = y;
		
		//top point		
		int x1 = x+(length/2);
		int y1 = (int) (y-(Math.sqrt(3)*length/2));
		
		recursiveTriangles(n, x1, y1, x, y, x3, y3);
	}
	
	/*
	 * method for drawing a Sierpinski
	 * input is the number of levels and the vertices
	 */
	public void recursiveTriangles(int n, int x1, int y1, int x2, int y2, int x3, int y3){
		//basecase - if level is 0 - do nothing
		if(n==0){return;}
		else{
			n--;
			//color for the inner circle (changes randomly for every created triangle - use wait() to see the result)
//			setForegroundColor(createRandomColor());
//			//this is the inner circle - depends on the canvassize
//			drawTriangleAsShape(canvas.getWidth()/2, canvas.getHeight() , 
//								canvas.getWidth()/4, canvas.getHeight()/2, 
//								3*canvas.getWidth()/4, canvas.getHeight()/2);
			
//			wait(15);
			
			//generate a color for the circle - depends on the level (n)
			setForegroundColor(getAColor(n));
			//draw the current triangle
			drawTriangleAsShape(x1,y1,x2,y2,x3,y3);
			
			//go one step down - draw the other triangles by bisection of the lengths / vertices
			recursiveTriangles(n, x1, y1, (x1+x2)/2, (y1+y2)/2, (x1+x3)/2, (y1+y3)/2); //top
			
			recursiveTriangles(n,(x1+x2)/2, (y1+y2)/2, x2, y2, (x2+x3)/2, (y2+y3)/2); //left
		
			recursiveTriangles(n,(x1+x3)/2, (y1+y3)/2, (x2+x3)/2, (y2+y3)/2, x3, y3); //right			
		}		
	}
	
	/*
	 * method for generating a color which depends on 
	 * the parameter (should represent the level)
	 */
	public Color getAColor(int n){
		if(n==0)return Color.black;
		if(n==1)return Color.blue;
		if(n==2)return Color.CYAN;
		if(n==3)return Color.pink;
		if(n==4)return Color.MAGENTA;
		if(n==5)return Color.YELLOW;
		return Color.gray; 		
	}
	 
	 /*
	  * method for generating a random color
	  */
	 public Color createRandomColor(){
	    Random random = new Random();
	    int red = random.nextInt(255);
	    int green = random.nextInt(255);
	    int blue = random.nextInt(255);
	    Color randomColour = new Color(red,green,blue);
	    return randomColour;
     }
	 
	 /*
	  * method for taking care of the componentlistener
	  * this sets a new size, fills the canvas with white and 
	  * draw a new Sierpinski depending on the parameters
	  */
	 public void changingSize(int x, int y){
		 setSize(x, y);
		 setForegroundColor(Color.white);
		 fillRectangle(0, 0, x, y);
		 
		 //vertical window
		 if(y>(Math.sqrt(3)*x/2)){
			 recursiveEquilateralTriangles(6, 0, y, x);
		 }
		 //horizontal window
		 else{
			 //here the length gets calculated with the height value
			 recursiveEquilateralTriangles(6, 0, y, (int) (2*y/Math.sqrt(3)));
		 }
		 
//		 recursiveTriangles(6,
//					x/2, 0, 	//unten 
//					0, y, 		//rechts
//					x, y);		//links
	 }
}
